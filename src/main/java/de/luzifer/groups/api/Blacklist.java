package de.luzifer.groups.api;

import de.luzifer.groups.Groups;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class Blacklist {

    static File file = new File("plugins/Groups" , "blacklist.yml");
    static FileConfiguration cfg;

    static {

        if(!file.exists()) {

            try {
                PrintWriter pw = new PrintWriter(file);

                pw.print(IOUtils.toString(Objects.requireNonNull(Groups.getInstance().getResource("blacklist.yml"))));
                pw.flush();
                pw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        cfg = YamlConfiguration.loadConfiguration(file);

        try {
            cfg.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

    }

    public static List<String> getForbidden() {

        return cfg.getStringList("Forbidden-Namens");

    }

    public static boolean isForbidden(String name) {

        for(String s : cfg.getStringList("Forbidden-Names")) {

            if(s.equalsIgnoreCase(name)) {

                return true;

            }

        }
        return false;
    }

}
