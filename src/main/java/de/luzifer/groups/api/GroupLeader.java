package de.luzifer.groups.api;

public interface GroupLeader {

    User asUser();

}
