package de.luzifer.groups.api.events;

import de.luzifer.groups.api.Group;
import de.luzifer.groups.api.User;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupInviteEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();
    private boolean isCancelled;

    private final User target;
    private final User user;
    private final Group group;

    public GroupInviteEvent(User user,User target, Group group) {

        this.user = user;
        this.group = group;
        this.target = target;

    }

    public User getTarget() {

        return target;

    }

    public User getUser() {

        return user;

    }

    public Group getGroup() {

        return group;

    }

    public boolean isCancelled() {

        return isCancelled;

    }

    public void setCancelled(boolean cancelled) {

        isCancelled = cancelled;

    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {

        return HANDLERS;

    }

}
