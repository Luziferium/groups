package de.luzifer.groups.api.events;

import de.luzifer.groups.api.Group;
import de.luzifer.groups.api.GroupModerator;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupUserDemoteEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();
    private boolean isCancelled;

    private final GroupModerator user;
    private final Group group;

    public GroupUserDemoteEvent(GroupModerator groupModerator, Group group) {

        this.user = groupModerator;
        this.group = group;

    }

    public GroupModerator getDemote() {

        return user;

    }

    public Group getGroup() {

        return group;

    }

    public boolean isCancelled() {

        return isCancelled;

    }

    public void setCancelled(boolean cancelled) {

        isCancelled = cancelled;

    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {

        return HANDLERS;

    }

}
