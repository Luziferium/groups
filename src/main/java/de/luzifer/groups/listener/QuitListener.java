package de.luzifer.groups.listener;

import de.luzifer.groups.api.User;
import de.luzifer.groups.api.events.GroupLeaveEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player player = e.getPlayer();
        User user = User.get(player.getUniqueId());

        if(user.hasGroup()) {

            GroupLeaveEvent groupLeaveEvent = new GroupLeaveEvent(user, user.getGroup(), "§e" + user.getName() + " left the group", false);
            Bukkit.getPluginManager().callEvent(groupLeaveEvent);

            if(!groupLeaveEvent.isCancelled()) {

                if(groupLeaveEvent.getGroup().getGroupLeader() == user) {

                    groupLeaveEvent.getGroup().setGroupLeader(null);

                }

                for(User groupMember : groupLeaveEvent.getGroup().getMembers()) {

                    groupMember.getPlayer().sendMessage(groupLeaveEvent.getLeaveMessage());

                }

                groupLeaveEvent.getGroup().removeMember(user);

                if(groupLeaveEvent.getGroup().getMembers().isEmpty()) {

                    groupLeaveEvent.getGroup().delete();

                }

            }

        }

    }

}
