package de.luzifer.groups.api.events;


import de.luzifer.groups.Groups;
import de.luzifer.groups.api.Group;
import de.luzifer.groups.api.User;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupLeaveEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();
    private boolean isCancelled;

    private final User user;
    private final Group group;

    private boolean kicked;

    private String leaveMessage;

    public GroupLeaveEvent(User user, Group group, String leaveMessage, boolean kicked) {

        this.user = user;
        this.group = group;
        this.leaveMessage = leaveMessage;
        this.kicked = kicked;

    }

    public boolean isKicked() {

        return kicked;

    }

    public void setLeaveMessage(String leaveMessage) {

        this.leaveMessage = leaveMessage;

    }

    public String getLeaveMessage() {

        return "§8[§a" + group.getName() + "§8] " + leaveMessage;

    }

    public User getUser() {

        return user;

    }

    public Group getGroup() {

        return group;

    }

    public boolean isCancelled() {

        return isCancelled;

    }

    public void setCancelled(boolean cancelled) {

        isCancelled = cancelled;

    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {

        return HANDLERS;

    }
}
