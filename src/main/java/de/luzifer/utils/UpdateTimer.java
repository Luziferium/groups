package de.luzifer.utils;

import de.luzifer.groups.Groups;
import org.bukkit.Bukkit;

public class UpdateTimer implements Runnable {

    @Override
    public void run() {

        new UpdateChecker(Groups.getInstance(), 83668).getVersion(version -> {
            if (!Groups.getInstance().getDescription().getVersion().equals(version)) {
                Bukkit.broadcastMessage(" ");
                Bukkit.broadcastMessage(Groups.prefix + "§aAn update is available");
                Bukkit.broadcastMessage(Groups.prefix + "§aGet it here");
                Bukkit.broadcastMessage("§3https://www.spigotmc.org/resources/groups-1-8-x-1-16-2.83668/");
                Bukkit.broadcastMessage(" ");
            }
        });

    }

}
