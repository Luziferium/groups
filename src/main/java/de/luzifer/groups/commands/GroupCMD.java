package de.luzifer.groups.commands;

import de.luzifer.groups.Groups;
import de.luzifer.groups.api.Blacklist;
import de.luzifer.groups.api.Group;
import de.luzifer.groups.api.User;
import de.luzifer.groups.api.events.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

public class GroupCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {

            sender.sendMessage("Whuups.. That didnt worked!");
            return true;

        }

        Player player = (Player) sender;
        User user = User.get(player.getUniqueId());

        if(command.getName().equalsIgnoreCase("group")) {

            if(args.length == 0) {

                player.sendMessage("");
                player.sendMessage(Groups.prefix + "§7All Commands: §8Or use /grp");
                player.sendMessage(Groups.prefix + "§7- §6/group");
                player.sendMessage(Groups.prefix + "§7- §6/group info");
                player.sendMessage(Groups.prefix + "§7- §6/group leave");
                player.sendMessage(Groups.prefix + "§7- §6/group create <NAME>");
                player.sendMessage(Groups.prefix + "§7- §6/group join <NAME>");
                player.sendMessage(Groups.prefix + "§7- §6/group invite <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group kick <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group promote <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group demote <PLAYER>");
                player.sendMessage("");

            }
            else if(args.length == 1) {

                if(args[0].equalsIgnoreCase("info")) {

                    if(!user.hasGroup()) {
                        player.sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;
                    }

                    player.sendMessage("");
                    player.sendMessage(Groups.prefix + "§7Name : §a" + user.getGroup().getName());
                    player.sendMessage(Groups.prefix + "§7Chatprefix : §a" + Groups.groupChatPrefix + "§8 Example: " + Groups.groupChatPrefix + "hello group!");
                    player.sendMessage(Groups.prefix + "§7Leader : §a" + user.getGroup().getGroupLeader().asUser().getName());
                    player.sendMessage(Groups.prefix + "§7Members : §a" + user.getGroup().getGroupSize() + "§7/§c" + Groups.groupSize);
                    for(User groupMember : user.getGroup().getMembers()) {

                        if(groupMember == user.getGroup().getGroupLeader()) {

                            player.sendMessage(Groups.prefix + "§7- §e" + groupMember.getName());

                        } else if(groupMember.getGroup().isPromoted(groupMember)){

                            player.sendMessage(Groups.prefix + "§7- §3" + groupMember.getName());

                        } else {

                            player.sendMessage(Groups.prefix + "§7- §a" + groupMember.getName());

                        }

                    }
                    player.sendMessage("");

                }
                else if(args[0].equalsIgnoreCase("leave")) {

                    if(!user.hasGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;

                    }

                    GroupLeaveEvent groupLeaveEvent = new GroupLeaveEvent(user, user.getGroup(), "§e" + user.getName() + " left the group", false);
                    Bukkit.getPluginManager().callEvent(groupLeaveEvent);

                    if(!groupLeaveEvent.isCancelled()) {

                        if(groupLeaveEvent.getGroup().getGroupLeader() == groupLeaveEvent.getUser()) {

                            groupLeaveEvent.getGroup().setGroupLeader(null);

                        }

                        if(groupLeaveEvent.getGroup().isPromoted(groupLeaveEvent.getUser())) {

                            groupLeaveEvent.getGroup().groupModerators().remove(groupLeaveEvent.getUser());

                        }

                        for(User groupMember : groupLeaveEvent.getGroup().getMembers()) {

                            groupMember.getPlayer().sendMessage(groupLeaveEvent.getLeaveMessage());

                        }

                        groupLeaveEvent.getGroup().removeMember(groupLeaveEvent.getUser());

                        if(groupLeaveEvent.getGroup().getMembers().isEmpty()) {

                            groupLeaveEvent.getGroup().delete();

                        }

                    }

                }
                else {
                    player.sendMessage("");
                    player.sendMessage(Groups.prefix + "§7All Commands: §8Or use /grp");
                    player.sendMessage(Groups.prefix + "§7- §6/group");
                    player.sendMessage(Groups.prefix + "§7- §6/group info");
                    player.sendMessage(Groups.prefix + "§7- §6/group leave");
                    player.sendMessage(Groups.prefix + "§7- §6/group create <NAME>");
                    player.sendMessage(Groups.prefix + "§7- §6/group join <NAME>");
                    player.sendMessage(Groups.prefix + "§7- §6/group invite <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group kick <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group promote <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group demote <PLAYER>");
                    player.sendMessage("");
                }

            }
            else if(args.length == 2) {

                if(args[0].equalsIgnoreCase("demote")) {

                    Player target = Bukkit.getPlayer(args[1]);

                    if(target == null) {
                        player.sendMessage(Groups.prefix + "§7This player is offline!");
                        return true;

                    }

                    User targetUser = User.get(target.getUniqueId());

                    if(targetUser == user) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't demote yourself!");
                        return true;

                    }

                    if(!user.hasGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;

                    }

                    if(!(user.getGroup().getGroupLeader() == user)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You are not the GroupLeader!");
                        return true;
                    }

                    if(!targetUser.hasGroup() || targetUser.getGroup() != user.getGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is not in your group!");
                        return true;
                    }

                    if(!targetUser.getGroup().isPromoted(targetUser)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is not a moderator!");
                        return true;
                    }

                    GroupUserDemoteEvent demote = new GroupUserDemoteEvent(targetUser, user.getGroup());
                    Bukkit.getPluginManager().callEvent(demote);

                    if(!demote.isCancelled()) {

                        demote.getGroup().demote(demote.getDemote().asUser());

                        for(User gmember : demote.getGroup().getMembers()) {

                            gmember.getPlayer().sendMessage("§8[§a" + demote.getGroup().getName() + "§8] §3" + demote.getDemote().asUser().getName() + "§e has been demoted!");

                        }

                    }

                }
                else
                if(args[0].equalsIgnoreCase("promote")) {

                    Player target = Bukkit.getPlayer(args[1]);

                    if(target == null) {
                        player.sendMessage(Groups.prefix + "§7This player is offline!");
                        return true;

                    }

                    User targetUser = User.get(target.getUniqueId());

                    if(targetUser == user) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't promote yourself!");
                        return true;

                    }

                    if(!user.hasGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;

                    }

                    if(!(user.getGroup().getGroupLeader() == user)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You are not the GroupLeader!");
                        return true;
                    }

                    if(!targetUser.hasGroup() || targetUser.getGroup() != user.getGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is not in your group!");
                        return true;
                    }

                    if(targetUser.getGroup().isPromoted(targetUser)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is already a moderator!");
                        return true;
                    }

                    GroupUserPromoteEvent promote = new GroupUserPromoteEvent(targetUser, user.getGroup());
                    Bukkit.getPluginManager().callEvent(promote);

                    if(!promote.isCancelled()) {

                        promote.getGroup().promote(promote.getPromote().asUser());

                        for(User gmember : promote.getGroup().getMembers()) {

                            gmember.getPlayer().sendMessage("§8[§a" + promote.getGroup().getName() + "§8] §3" + promote.getPromote().asUser().getName() + "§e has been promoted!");

                        }

                    }

                }
                else
                if(args[0].equalsIgnoreCase("invite")) {

                    Player target = Bukkit.getPlayer(args[1]);

                    if(target == null) {
                        player.sendMessage(Groups.prefix + "§7This player is offline!");
                        return true;

                    }

                    User targetUser = User.get(target.getUniqueId());

                    if(targetUser == user) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't invite yourself!");
                        return true;

                    }

                    if(!user.hasGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;

                    }

                    if(!(user.getGroup().getGroupLeader() == user) && !user.getGroup().isPromoted(user)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You are not the GroupLeader or a GroupModerator!");
                        return true;
                    }

                    if(user.getGroup().getGroupSize() >= Groups.groupSize) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7This group is full!");
                        return true;

                    }

                    if(targetUser.hasGroup()) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is in a group!");
                        return true;

                    }

                    if(targetUser.hasGroupInvite(user.getGroup())) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7This player has already a group invite!");
                        return true;

                    }

                    GroupInviteEvent groupInviteEvent = new GroupInviteEvent(user, targetUser, user.getGroup());
                    Bukkit.getPluginManager().callEvent(groupInviteEvent);

                    if(!groupInviteEvent.isCancelled()) {

                        groupInviteEvent.getTarget().addGroupInvite(groupInviteEvent.getUser().getGroup());
                        groupInviteEvent.getTarget().getPlayer().sendMessage(Groups.prefix + "§7You've been invited to §a" + groupInviteEvent.getGroup().getName());
                        groupInviteEvent.getTarget().getPlayer().sendMessage(Groups.prefix + "§7Use §6/group join §a" + groupInviteEvent.getGroup().getName() + "§7 to join.");
                        groupInviteEvent.getUser().getPlayer().sendMessage(Groups.prefix + "§a" + groupInviteEvent.getTarget().getName() + "§7 have been invited!");

                    }

                }
                else

                if(args[0].equalsIgnoreCase("join")) {

                    String name = args[1].toUpperCase();

                    if(user.hasGroup()) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You are in a group!");
                        return true;

                    }

                    if(!user.hasGroupInvite(Group.name(name))) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You are not invited!");
                        return true;

                    }

                    if(Group.name(name).getGroupSize() >= Groups.groupSize) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7This group is full!");
                        return true;

                    }

                    GroupJoinEvent groupJoinEvent = new GroupJoinEvent(user, Group.name(name), "§e" + user.getName() + " joined the group");
                    Bukkit.getPluginManager().callEvent(groupJoinEvent);

                    if(!groupJoinEvent.isCancelled()) {

                        groupJoinEvent.getGroup().addMember(groupJoinEvent.getUser());

                        groupJoinEvent.getUser().getGroupInvites().clear();

                        for(User groupMember : groupJoinEvent.getGroup().getMembers()) {

                            groupMember.getPlayer().sendMessage(groupJoinEvent.getJoinMessage());

                        }

                    }

                }
                else

                if(args[0].equalsIgnoreCase("create")) {

                    if(!Groups.getInstance().getConfig().getString("Create-Group-Permission").equalsIgnoreCase("none")) {

                        if(!player.hasPermission(Objects.requireNonNull(Groups.getInstance().getConfig().getString("Create-Group-Permission"))) && !player.isOp()) {

                            player.sendMessage(Groups.prefix + "§7You don't have the permission to do that!");
                            return true;

                        }

                    }

                    String name = args[1].toUpperCase();

                    if(name.length() > Groups.groupNameLength) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7The name may only be " + Groups.groupNameLength +" characters long!");
                        return true;

                    }

                    if(Blacklist.isForbidden(name)) {

                        user.getPlayer().sendMessage(Groups.prefix + "§c" + name + "§7 is not allowed!");
                        return true;

                    }

                    if(user.hasGroup()) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You are in a group!");
                        return true;

                    }

                    if(Group.alreadyExist(name)) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7A group with this name already exist!");
                        return true;

                    }

                    GroupCreateEvent groupCreateEvent = new GroupCreateEvent(user);
                    Bukkit.getPluginManager().callEvent(groupCreateEvent);

                    if(!groupCreateEvent.isCancelled()) {

                        Group group = Group.name(name);
                        group.addMember(groupCreateEvent.getGroupLeader().asUser());
                        group.setGroupLeader(groupCreateEvent.getGroupLeader().asUser());

                        groupCreateEvent.getGroupLeader().asUser().getPlayer().sendMessage(Groups.prefix + "§7You successfully created §a" + name);

                    }

                }
                else if(args[0].equalsIgnoreCase("kick")) {

                    Player target = Bukkit.getPlayer(args[1]);

                    if(target == null) {

                        player.sendMessage(Groups.prefix + "§7This player is offline!");
                        return true;

                    }

                    User targetUser = User.get(target.getUniqueId());

                    if(targetUser == user) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't kick yourself!");
                        return true;

                    }

                    if(!user.hasGroup()) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You don't have a group!");
                        return true;

                    }

                    if(!(user.getGroup().getGroupLeader() == user) && !user.getGroup().isPromoted(user)) {
                        user.getPlayer().sendMessage(Groups.prefix + "§7You are not the GroupLeader or a GroupModerator!");
                        return true;
                    }

                    if(!targetUser.hasGroup() || targetUser.getGroup() != user.getGroup()) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7This player is not in the group!");
                        return true;

                    }

                    if(targetUser.getGroup().isPromoted(targetUser)) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't kick a GroupModerator!");
                        return true;

                    }

                    if(targetUser.getGroup().getGroupLeader() == targetUser) {

                        user.getPlayer().sendMessage(Groups.prefix + "§7You can't kick the GroupLeader!");
                        return true;

                    }

                    GroupLeaveEvent groupLeaveEvent = new GroupLeaveEvent(targetUser, user.getGroup(), "§e" + targetUser.getName() + " has been kicked out of the group", true);
                    Bukkit.getPluginManager().callEvent(groupLeaveEvent);

                    if(!groupLeaveEvent.isCancelled()) {

                        if(groupLeaveEvent.getGroup().getGroupLeader() == groupLeaveEvent.getUser()) {

                            groupLeaveEvent.getGroup().setGroupLeader(null);

                        }

                        if(groupLeaveEvent.getGroup().isPromoted(groupLeaveEvent.getUser())) {

                            groupLeaveEvent.getGroup().groupModerators().remove(groupLeaveEvent.getUser());

                        }

                        for(User groupMember : groupLeaveEvent.getGroup().getMembers()) {

                            groupMember.getPlayer().sendMessage(groupLeaveEvent.getLeaveMessage());

                        }

                        groupLeaveEvent.getGroup().removeMember(groupLeaveEvent.getUser());

                    }

                }
                else {
                    player.sendMessage("");
                    player.sendMessage(Groups.prefix + "§7All Commands: §8Or use /grp");
                    player.sendMessage(Groups.prefix + "§7- §6/group");
                    player.sendMessage(Groups.prefix + "§7- §6/group info");
                    player.sendMessage(Groups.prefix + "§7- §6/group leave");
                    player.sendMessage(Groups.prefix + "§7- §6/group create <NAME>");
                    player.sendMessage(Groups.prefix + "§7- §6/group join <NAME>");
                    player.sendMessage(Groups.prefix + "§7- §6/group invite <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group kick <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group promote <PLAYER>");
                    player.sendMessage(Groups.prefix + "§7- §6/group demote <PLAYER>");
                    player.sendMessage("");
                }

            }
            else {
                player.sendMessage("");
                player.sendMessage(Groups.prefix + "§7All Commands: §8Or use /grp");
                player.sendMessage(Groups.prefix + "§7- §6/group");
                player.sendMessage(Groups.prefix + "§7- §6/group info");
                player.sendMessage(Groups.prefix + "§7- §6/group leave");
                player.sendMessage(Groups.prefix + "§7- §6/group create <NAME>");
                player.sendMessage(Groups.prefix + "§7- §6/group join <NAME>");
                player.sendMessage(Groups.prefix + "§7- §6/group invite <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group kick <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group promote <PLAYER>");
                player.sendMessage(Groups.prefix + "§7- §6/group demote <PLAYER>");
                player.sendMessage("");
            }

        }

        return false;
    }
}
