package de.luzifer.groups.listener;

import de.luzifer.groups.Groups;
import de.luzifer.groups.api.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;


public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player player = e.getPlayer();
        User user = User.get(player.getUniqueId());
        String msg = e.getMessage();

        if(msg.startsWith(Groups.groupChatPrefix)) {

            e.setCancelled(true);

            if(!user.hasGroup()) {

                user.getPlayer().sendMessage(Groups.prefix + "§7You are not in a group!");
                return;

            }

            for(User groupMember : user.getGroup().getMembers()) {

                if(user.getGroup().getGroupLeader() == user) {

                    groupMember.getPlayer().sendMessage("§8[§a" + user.getGroup().getName() + "§8] §e" + user.getName() + " §7-> §3" + e.getMessage().replace(Groups.groupChatPrefix, ""));

                } else if(user.getGroup().isPromoted(user) ){

                    groupMember.getPlayer().sendMessage("§8[§a" + user.getGroup().getName() + "§8] §3" + user.getName() + " §7-> §3" + e.getMessage().replace(Groups.groupChatPrefix, ""));

                } else {

                    groupMember.getPlayer().sendMessage("§8[§a" + user.getGroup().getName() + "§8] §a" + user.getName() + " §7-> §a" + e.getMessage().replace(Groups.groupChatPrefix, ""));

                }

            }

        }

    }

}
