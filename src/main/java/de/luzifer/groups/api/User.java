package de.luzifer.groups.api;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User implements GroupLeader, GroupModerator {

    private static ArrayList<User> allUser = new ArrayList<>();
    private ArrayList<Group> groupInvites = new ArrayList<>();

    private String normalPlayerListName;

    private UUID uuid;

    private User(UUID uuid) {

        this.uuid = uuid;

    }

    public UUID getUniqueID() {

        return this.uuid;

    }

    public void setNormalPlayerListName(String name) {

        normalPlayerListName = name;

    }

    public String getNormalPlayerListName() {

        return normalPlayerListName;

    }

    public String getName() {

        return Bukkit.getOfflinePlayer(uuid).getName();

    }

    public Player getPlayer() {

        return Bukkit.getPlayer(uuid);

    }

    public OfflinePlayer getOfflinePlayer() {

        return Bukkit.getOfflinePlayer(uuid);

    }

    public static User get(UUID uuid) {

        for(User user : allUser) {

            if(user.getUniqueID().equals(uuid)) {

                return user;

            }

        }

        User user = new User(uuid);
        getAllUser().add(user);
        return user;

    }

    public static List<User> getAllUser() {

        return allUser;

    }

    public boolean hasGroup() {

        for(Group group : Group.getAll()) {

            if(group.containsMember(this)) {

                return true;

            }

        }
        return false;
    }

    public Group getGroup() {

        for(Group group : Group.getAll()) {

            if(group.containsMember(this)) {

                return group;

            }

        }
        return null;
    }

    public void setGroup(Group group) {

        if(getGroup() != null) {

            getGroup().removeMember(this);

        }

        group.addMember(this);

    }

    public List<Group> getGroupInvites() {

        return groupInvites;

    }

    public void addGroupInvite(Group group) {

        getGroupInvites().add(group);

    }

    public void removeGroupInvite(Group group) {

        getGroupInvites().remove(group);

    }

    public boolean hasGroupInvite(Group group) {

        if(getGroupInvites().contains(group)) {

            return true;

        }
        return false;
    }

    @Override
    public User asUser() {
        return User.get(uuid);
    }
}
