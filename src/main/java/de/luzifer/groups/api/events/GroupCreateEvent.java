package de.luzifer.groups.api.events;

import de.luzifer.groups.api.GroupLeader;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupCreateEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();
    private boolean isCancelled;

    private final GroupLeader user;

    public GroupCreateEvent(GroupLeader groupLeader) {

        this.user = groupLeader;

    }

    public GroupLeader getGroupLeader() {

        return user;

    }

    public boolean isCancelled() {

        return isCancelled;

    }

    public void setCancelled(boolean cancelled) {

        isCancelled = cancelled;

    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {

        return HANDLERS;

    }


}
