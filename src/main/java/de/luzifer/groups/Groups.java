package de.luzifer.groups;

import de.luzifer.groups.api.Group;
import de.luzifer.groups.api.User;
import de.luzifer.groups.api.events.GroupDeleteEvent;
import de.luzifer.groups.commands.GroupCMD;
import de.luzifer.groups.listener.ChatListener;
import de.luzifer.groups.listener.QuitListener;
import de.luzifer.utils.NameUpdater;
import de.luzifer.utils.UpdateTimer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class Groups extends JavaPlugin {

    public static String prefix;

    public static String groupChatPrefix;
    public static int groupNameLength;
    public static int groupSize;

    @Override
    public void onEnable() {

        prefix = "§8[§aGC§8] ";
        instance = this;
        initPlayer();
        loadConfig();
        loadCommands();
        loadListener();
        groupChatPrefix = getConfig().getString("Group-Chat-Prefix");
        groupNameLength = getConfig().getInt("Max-Group-Name-Length");
        groupSize = getConfig().getInt("Max-Group-Members");

        if(getConfig().getBoolean("UpdateChecker")) {

            Bukkit.getScheduler().runTaskTimer(this, new UpdateTimer(), 0, 20*60*60*3);

        }

        if(getConfig().getBoolean("Show-Group-In-Tablist")) {

            Bukkit.getScheduler().runTaskTimer(this, new NameUpdater(), 0, 20*30);

        }

    }

    public void loadConfig() {

        getConfig().options().copyDefaults();
        saveDefaultConfig();

    }

    @Override
    public void onDisable() {

        if(!Group.getAll().isEmpty()) {

            for(Group group : Group.getAll()) {

                GroupDeleteEvent event = new GroupDeleteEvent(group);
                Bukkit.getPluginManager().callEvent(event);
                if(!event.isCancelled()) {

                    for(User user : event.getGroup().getMembers()) {

                        user.getPlayer().sendMessage(prefix + "§7This group has been deleted!");
                        user.getPlayer().setPlayerListName(user.getNormalPlayerListName());

                    }

                }

            }

        }

    }

    public void initPlayer() {

        for(Player all : Bukkit.getOnlinePlayers()) {

            User.get(all.getUniqueId());

        }

    }

    public void loadListener() {

        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new QuitListener(), this);

    }

    public void loadCommands() {

        Objects.requireNonNull(getCommand("group")).setExecutor(new GroupCMD());

    }

    public static Groups instance;
    public static Groups getInstance() {

        return instance;

    }

}
