package de.luzifer.groups.api;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Group {

    private static ArrayList<Group> groups = new ArrayList<>();

    private String name;
    private GroupLeader leader;
    private ArrayList<User> members = new ArrayList<>();
    private ArrayList<GroupModerator> moderators = new ArrayList<>();
    private UUID uuid;

    public Group(String name) {

        this.name = name;
        this.uuid = UUID.randomUUID();

    }

    public static Group name(String name) {

        for(Group group : groups) {

            if(group.getName().equals(name)) {

                return group;

            }

        }

        Group group = new Group(name);
        groups.add(group);
        return group;

    }

    public static boolean alreadyExist(String name) {

        if(!Group.getAll().isEmpty()) {

            for(Group group : Group.getAll()) {

                if(group.getName().equalsIgnoreCase(name)) {

                    return true;

                }

            }

        }
        return false;
    }

    public static ArrayList<Group> getAll() {

        return groups;

    }

    public UUID getUniqueId() {

        return uuid;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public ArrayList<User> getMembers() {

        return members;

    }

    public void addMember(User user) {

        members.add(user);

    }

    public void removeMember(User user) {

        members.remove(user);

    }

    public boolean containsMember(User user) {

        return members.contains(user);

    }

    public void delete() {

        getMembers().clear();
        Group.getAll().remove(this);

    }

    public GroupLeader getGroupLeader() {

        if(leader != null) {

            return leader;

        }

        if(!groupModerators().isEmpty()) {

            return moderators.get(0);

        }

        return members.get(0);

    }

    public void promote(User user) {

        moderators.add(user);

    }

    public boolean isPromoted(User user) {

        return moderators.contains(user);

    }

    public void demote(User user) {

        moderators.remove(user);

    }

    public List<GroupModerator> groupModerators() {

        return moderators;

    }

    public void setGroupLeader(User user) {

        this.leader = user;

    }

    public int getGroupSize() {

        return members.size();

    }

}
