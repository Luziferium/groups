package de.luzifer.groups.api;

public interface GroupModerator extends GroupLeader {

    User asUser();

}
