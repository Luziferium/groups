package de.luzifer.utils;

import de.luzifer.groups.api.User;
import org.bukkit.entity.Player;

public class NameUpdater implements Runnable {

    @Override
    public void run() {

        for(User user : User.getAllUser()) {

            if(user.hasGroup()) {

                Player player = user.getPlayer();

                if(user.getNormalPlayerListName() == null) {

                    user.setNormalPlayerListName(player.getPlayerListName());

                }

                if(user.getGroup().getGroupLeader() == user) {

                    player.setPlayerListName(user.getNormalPlayerListName() + " §8[§e" + user.getGroup().getName() + "§8]");

                } else if(user.getGroup().isPromoted(user)) {

                    player.setPlayerListName(user.getNormalPlayerListName() + " §8[§3" + user.getGroup().getName() + "§8]");

                } else {

                    player.setPlayerListName(user.getNormalPlayerListName() + " §8[§a" + user.getGroup().getName() + "§8]");

                }

            } else {

                if(user.getNormalPlayerListName() != null) {

                    Player player = user.getPlayer();
                    player.setPlayerListName(user.getNormalPlayerListName());
                    user.setNormalPlayerListName(null);

                }

            }

        }

    }

}
